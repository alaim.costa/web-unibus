import React from 'react'

import Image from 'next/image'

export default function Login() {
  return (
    <div className='container'>
      <div className='inicial'>
      </div>
      
      <div className='login-usuario'>
        <div className='imagem-login'>
          <img id="img01" src="/img/unibus_logo02.png" alt="Logo do aplicativo da rota universitária UNIBUS"/>
          <img id="img02" src="/img/logounifesspa.png" alt="Logo da Unifesspa" />
        </div>
        
        <form action="">
          <input type="text" name='emailLogin' placeholder='Nome do Usuário'/><br />
          <input type="password" name='passwordLogin' placeholder='Senha'/><br />
          <button>Entrar</button>

        </form>
        <p>Não possui acesso?<strong> <a href="#">Solicitar</a></strong></p>
        
        
      </div>


    </div>
  )
}
